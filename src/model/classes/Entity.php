<?php
namespace SI5_TP_17\model\classes;

use Exception;

abstract class Entity
{
    public function __set($name, $value)
    {
        $class = get_class($this);
        $method = "set_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not writable in $class.");
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $class = get_class($this);
        $method = "get_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not readable in $class.");
        }
        return $this->$method();
    }
}