<?php
namespace SI5_TP_17\view;

class View
{
    private static $_name;
    private static $_params = [];

    public static function bindParam($name, $value)
    {
        self::$_params[$name] = $value;
    }

    public static function setTemplate($name)
    {
        self::$_name = $name;
    }

    public static function display()
    {
        foreach(self::$_params as $name => $value)
        {
            $$name = $value;
        }
        $controller = filter_input(INPUT_GET, 'controller', FILTER_SANITIZE_STRING);
        $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

        $template = self::$_name . ".html.php";
        $script = self::$_name . ".js";

        require_once "src/view/templates/index.html.php";
    }
}