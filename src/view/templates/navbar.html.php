<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href=".">SI5-TP-17</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item<?= ($controller == 'home')?" active":""; ?>">
        <a class="nav-link" href="?controller=home&action=default">Home</a>
      </li>
      <li class="nav-item<?= ($controller == 'personne' && $action == 'list')?" active":""; ?>">
        <a class="nav-link" href="?controller=personne&action=list">Personnes</a>
      </li>
    </ul>
  </div>
</nav>