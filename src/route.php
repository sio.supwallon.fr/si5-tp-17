<?php
namespace SI5_TP_17;

use SI5_TP_17\controllers\HomeController;
use SI5_TP_17\controllers\PersonneController;

$controller = filter_input(INPUT_GET, 'controller', FILTER_SANITIZE_STRING);

switch($controller)
{
    case 'home':
        HomeController::route();
    break;
    case 'personne':
        PersonneController::route();
    break;
    default:
        HomeController::route();
    break;
}