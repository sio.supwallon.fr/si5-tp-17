<?php
namespace SI5_TP_17\controllers;

use SI5_TP_17\model\classes\Personne;
use SI5_TP_17\view\View;

class PersonneController
{
    public static function route()
    {
        $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

        switch($action)
        {
            case 'details':
                $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                self::details_action($id);
            break;
            case 'list':
                self::list_action();
            break;
        }
    }

    public static function list_action()
    {
        $personnes = Personne::getAll();

        View::setTemplate('personne_list');
        View::bindParam("personnes", $personnes);
        View::display();
    }

    public static function details_action($id)
    {
        $personne = Personne::get($id);

        View::setTemplate('personne_details');
        View::bindParam("personne", $personne);
        View::display();
    }
}